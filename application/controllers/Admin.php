<?php 

class Admin extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') == "login" && $this->session->userdata('role') == "1" ){
			
		}
		else{
			redirect(base_url("index.php/user"));
		}
		$this->load->helper('string');
		//$this->load->model('m_event');
		$this->load->model('m_user');
		$this->load->model('m_admin');
		$this->load->model('m_gerai');
		//$this->load->model('m_affiliate');
		//$this->load->model('m_admin');
	}
	function index(){
		
		$this->load->view('admin/admin_dashboard');
	}
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('index.php'));
	}
	function users($id=null){
		if($id==null){
			$data=$this->m_user->get_user();
			$this->load->view('admin/admin_users', array('data'=>$data));
		} 
		elseif($id !=null and ( $ban==1 or $ban==0 )) {
			
			$res1 = $this->db->update('user',$data_user,array('id'=>$id));
			if ($this->db->affected_rows()>0) {
				redirect('admin/admin_users');
			} 
			else{
				redirect('admin/admin_users');
			}
		}
	}
	function gerai($market_id=null){
		if ($market_id==null) {
			$get_gerai=$this->m_gerai->get_gerai_admin()->result_array();
			$this->load->view('admin/admin_gerai',array( 'data'=>$get_gerai));
		} else {
			$get_gerai=$this->m_gerai->get_gerai_admin($market_id)->result_array();
			$this->load->view('admin/admin_gerai_edit',array('data'=>$get_gerai));
		}
	}
	function gerai_tambah(){
		$this->load->view('admin/admin_tambah_gerai');
	}
	function gerai_edit($market_id){
		$id = array('market_id' => $market_id );	
		$data = array(
			'nama_minimarket' => $this->input->post("nama_minimarket",true),
			'alamat_minimarket' => $this->input->post("alamat_minimarket",true),
			'telp' => $this->input->post("telepon",true),
			'kota' => $this->input->post("kota",true),
			'lat' => $this->input->post("lat",true),
			'lng' => $this->input->post("lng",true)
		);
		$query = $this->m_gerai->update('gerai',$data,$id);
		if($query){
			redirect('admin/gerai');
		}
	}
	function hapus_gerai($market_id){
		$where = array('market_id' => $market_id);
		$this->m_gerai->hapus($where,'gerai');
		if($this->db->affected_rows()>0){
			$this->session->set_flashdata('pesan','Data Berhasil Dihapus.');
			redirect('admin/gerai');
		}
		else{
			$this->session->set_flashdata('pesan','Data gagal dihapus.');
			redirect('admin/gerai');
		}
	}
	function hapus_barang($id_barang){
		$where = array('id_barang' => $id_barang);
		$this->m_gerai->hapus($where,'barang');
		if($this->db->affected_rows()>0){
			$this->session->set_flashdata('pesan','Data Berhasil Dihapus.');
			redirect('admin/barang');
		}
		else{
			$this->session->set_flashdata('pesan','Data gagal dihapus.');
			redirect('admin/barang');
		}
	}
	function tambah_barang1($market_id){
		$get_barang=$this->m_gerai->get_gerai_admin($market_id)->result_array();
		$this->load->view('admin/admin_barang',array('data'=>$get_barang));
	}
	function barang($id_barang=null){
		if ($id_barang==null) {
			$get_barang=$this->m_gerai->get_barang_admin()->result_array();
			$this->load->view('admin/admin_barang1',array( 'data'=>$get_barang));
		} else {
			$get_barang=$this->m_gerai->get_barang_admin($id_barang)->result_array();
			$this->load->view('admin/admin_barang_edit',array('data'=>$get_barang));
		}
	}
	function barang_edit($id_barang){
		$id = array('id_barang' => $id_barang);
		$data = array(
			'nama_barang' => $this->input->post("nama_barang",true),
			'harga_barang' => $this->input->post("harga_barang",true),
			'harga_promo' => $this->input->post("harga_promo",true),
			'image' => $this->input->post("image",true),
			'keterangan' => $this->input->post("keterangan1",true),
		);
		$query = $this->m_gerai->update('barang',$data,$id);
		if($query){
			redirect('admin/barang');
		}
	}
	function edit_users($id,$edit=null){
		if ($edit==null){
			$data=$this->m_user->get_user_data($id);
			
			$this->load->view('admin/admin_edit_user',array('data'=>$data));
		} else if($edit=='y'){
			
			$data_user=array(
				'username'=>$_POST['username'],
				'email'=>$_POST['email'],
				'role'=>$_POST['role']
				// 'role'=>$_POST['role']
			);
			// $res1=$this->m_user->update_user('user_detail',$data_user_detail, array('user_id'=> $id));
			$res2=$this->m_user->update_user('user',$data_user, array('id'=> $id));
			if ($res2) {
				$this->session->set_flashdata('pesan','Data Berhasil Diperbarui');
				redirect(base_url('admin/edit_users/'.$id));
			} else {
				$this->session->set_flashdata('pesan','Data Gagal Diperbarui');
				redirect(base_url('admin/edit_users/'.$id));
			}
			
		}
	}

	function create_gerai(){
		$user_id 	= $userid=$this->session->userdata('id');

		$data_insert				= array(
			'user_id'			=> $user_id,
			'nama_minimarket'	=> $this->input->post('nama_minimarket'),
			'alamat_minimarket'	=> $this->input->post('alamat_minimarket'),
			'kota'				=> $this->input->post('kota'),
			'telp'				=> $this->input->post('telp')
			
		);

		$res = $this->m_admin->create_gerai($data_insert);
		
		if ($res) {
					redirect('admin/gerai'); //ke view halaman terimakasih
				} else {
					$this->session->set_flashdata('pesan','Isi Semua Form Dengan Benar!');
					redirect('admin/gerai');
				}
				
			}
			function tambah_barang($market_id){
				$id = $market_id;
				$data_insert= array(
					'market_id' => $id,
					'nama_barang' => $this->input->post('nama_barang'),
					'harga_barang' => $this->input->post('harga_barang'),
					'harga_promo' => $this->input->post('harga_promo'),
					'image' => $this->input->post('image'),
					'keterangan' => $this->input->post('keterangan1'),
				);
				$res = $this->m_gerai->tambah_barang($data_insert);

				if ($res){
					redirect('admin/barang');
				}
				else{
					$this->session->$this->session->set_flashdata('pesan', 'Isi Semua Form Dengan Benar!');
					redirect('admin/admin_gerai');
				}
			}
			
			
		}
		?>