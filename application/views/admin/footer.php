<!-- FOOTER -->
<!-- <footer id="main-footer">
  <p> Copyright AFFILIATE, &copy; 2017</p>
</footer> -->
<!-- END FOOTER -->
<!-- JS -->

<script src="<?php echo base_url() ?>assets/jquery/jquery-3.2.1.min.js" charset="utf-8"></script>
<script src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
<script src="<?php echo base_url() ?>assets/DataTables/datatables.min.js" charset="utf-8"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBkDOjo_qP1DP_PbJujie7n-Tyxzhdgfoo&callback=initMap">
    </script>
<script type="text/javascript">
  function goBack() {
    window.history.back();
  }

  $(document).ready(function(){
        //dashboard
        $("#table-lastpayment").DataTable({
          scrollX:true,
          searching: false,
          ordering:false,
          paginate:false,
          bInfo:false

        });
        $("#table-lastevent").DataTable({
          scrollX:true,
          searching: false,
          ordering:false,
          paginate:false,
          bInfo:false

        });
        // users
        $("#table-users").DataTable({
          scrollX:true,
          "aoColumnDefs": [
          {
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });

        // events
        $("#table-event-1").DataTable({
          "scrollX": true,
          "aoColumnDefs": [
          {
            "width": "150px",
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });
        $("#btn-createEvent").click(function(){
          $("#table-event").hide();
          $("#form-createEvent").fadeIn(500);
          $("#inputEvent").focus();
        });
        $("#btn-createEventReset").click(function(){
          $("#inputEvent").focus();
        });
        $("#btn-createEventCancel").click(function(){
          $("#form-createEvent").hide();
          $("#table-event").fadeIn(500);
        });
        $( "#tanggal" ).datepicker({
           dateFormat: "yy-mm-dd"
        });

        //tna
        $("#table-tna-1").DataTable({
          "scrollX": true,
          "aoColumnDefs": [
          {
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });

        //tutorial
        $("#table-tutorial-1").DataTable({
          "scrollX": true,
          "aoColumnDefs": [
          {
            "width": "150px",
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });

        //tutorial
        $("#table-benefit-1").DataTable({
          "scrollX": true,
          "aoColumnDefs": [
          {
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });
        $("#table-media-1").DataTable({
          "scrollX": true,
          "aoColumnDefs": [
          {
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });
        $("#btn-createTutorial").click(function(){
          $("#table-tutorial").hide();
          $("#form-createTutorial").fadeIn(500);
        });
        $("#btn-createTutorialCancel").click(function(){
          $("#form-createTutorial").hide();
          $("#table-tutorial").fadeIn(500);
        });

        //faq
        $("#table-faqs-1").DataTable({
          "scrollX": true,
          "aoColumnDefs": [
          {
            "width": "150px",
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });

        $("#btn-createFaqs").click(function(){
          $("#table-faqs").hide();
          $("#form-createFaqs").fadeIn(500);
        });
        $("#btn-createFaqsCancel").click(function(){
          $("#form-createFaqs").hide();
          $("#table-faqs").fadeIn(500);
        });

        //jenjang
        $("#table-jenjang-1").DataTable({
          "scrollX": true,
          "aoColumnDefs": [
          {
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });

        $("#btn-createJenjang").click(function(){
          $("#table-jenjang").hide();
          $("#form-createJenjang").fadeIn(500);
        });
        $("#btn-createJenjangCancel").click(function(){
          $("#form-createJenjang").hide();
          $("#table-jenjang").fadeIn(500);
        });

        //peraturan kerjasama
        $("#table-kerjasama-1").DataTable({
          "aoColumnDefs": [
          {
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });

        //mentoring
        $("#table-mentoring-1").DataTable({
          scrollX:true,
          "aoColumnDefs": [
          {
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });

        $("#btn-createMentoring").click(function(){
          $("#table-mentoring").hide();
          $("#form-createMentoring").fadeIn(500);
        });
        $("#btn-createMentoringCancel").click(function(){
          $("#form-createMentoring").hide();
          $("#table-mentoring").fadeIn(500);
        });

        // New User
        $("#table-new_user-1").DataTable({
          "aoColumnDefs": [
          {
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });

        // Pembayaran
        $("#table-pembayaran-1").DataTable({
          "scrollX": true,
          "aoColumnDefs": [
          {
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });

        // Top affiliate
        $("#table-top_affiliate-1").DataTable();
        //Membership
        $("#table-membership-1").DataTable({
          "scrollX": true,
          "aoColumnDefs": [
          {
            "bSortable": false,
            "aTargets": [ -1 ]
          }
          ]
        });

        //ckeditor
        CKEDITOR.replace('keterangan');
        ( "#datepicker" ).datepicker();
      });


    </script>
  </body>
  </html>
