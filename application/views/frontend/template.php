<!DOCTYPE HTML>
<html>
	<?php include 'header-front.php' ?>
	<body class="is-preload">
		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<div class="inner">

							<!-- Logo -->
								<a href="index.html" class="logo">
									<span class="symbol"><img src="images/logo.svg" alt="" /></span><span class="title">Skripsi Mantap</span>
								</a>

							<!-- Nav -->
								<nav>
									<ul>
										<li><a href="#menu">Menu</a></li>
									</ul>
								</nav>

						</div>
					</header>

				<!-- Menu -->
					<nav id="menu">
						<h2>Menu</h2>
						<ul>
							<li><a href="index.html">Home</a></li>
							<li><a href="generic.html">Ipsum veroeros</a></li>
							<li><a href="generic.html">Tempus etiam</a></li>
							<li><a href="generic.html">Consequat dolor</a></li>
							<li><a href="elements.html">Elements</a></li>
						</ul>
					</nav>

				<!-- Main -->
					<div id="main">
						<div class="inner">
							<header>
								<h1>Ini adalah skripsi<br />
								template diambil dari internet.</h1>
								<p>Skripsi ini dibuat dengan menggunakan usaha yang seadanya tapi berusaha untuk selesai jadi mohon di terima secara apa adanya karena cuma untuk kelulusan aja .</p>
							</header>
							<section class="tiles">
								<article class="style1">
									<span class="image">
										<img src="images/pic01.jpg" alt="" />
									</span>
									<a href="generic.html">
										<h2>Promo</h2>
										<div class="content">
											<p>Berisi barang apa saja yang lagi promo.</p>
										</div>
									</a>
								</article>
								<article class="style2">
									<span class="image">
										<img src="images/pic02.jpg" alt="" />
									</span>
									<a href="generic.html">
										<h2>Gerai</h2>
										<div class="content">
											<p>Berisi gerai apa saja yang terdaftar dalam aplikasi ini .</p>
										</div>
									</a>
								</article>
								<article class="style3">
									<span class="image">
										<img src="images/pic03.jpg" alt="" />
									</span>
									<a href="<?php echo base_url('frontend/map') ?>">
										<h2>Map</h2>
										<div class="content">
											<p>Menunjukkan map yang berisi lokasi gerai yang terdaftar dalam aplikasi.</p>
										</div>
									</a>
								</article>
								
							</section>
						</div>
					</div>

				<!-- Footer -->
					

			</div>

		<!-- Scripts -->
			<?php include 'footer-front.php' ?>

	</body>
</html>