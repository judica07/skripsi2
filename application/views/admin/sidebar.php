<div id="sidebar" class="col-md-3">
  <!-- Sidebar -->
  <div class="list-group">
    <a href="<?php echo base_url('admin') ?>" class="list-group-item">
      <span class="lnr lnr-cog"></span> Dashboard
    </a>
    <a href="<?php echo base_url('admin/users') ?>" class="list-group-item">
      <span class="lnr lnr-users"></span> Users
    </a>
    <a href="<?php echo base_url('admin/gerai') ?>" class="list-group-item">
      <span class="lnr lnr-bullhorn"></span> Gerai
    </a>
    <a href="<?php echo base_url('admin/barang') ?>" class="list-group-item">
      <span class="lnr lnr-file-add"></span> Barang
    </a>
  </div>
</div>
