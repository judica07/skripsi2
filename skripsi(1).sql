-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Jul 2019 pada 14.18
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `market_id` int(11) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `harga_barang` varchar(200) NOT NULL,
  `harga_promo` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id_barang`, `market_id`, `nama_barang`, `harga_barang`, `harga_promo`, `image`, `keterangan`) VALUES
(1, 31, 'baju', '5000', '2000', 'kamak', 'keterangan'),
(2, 31, 'baju', '650', '651', 'ajsb', 'lllll'),
(3, 31, 'kaka', '52', '25', 'makan', 'coba'),
(5, 36, 'ka', '88', '77', '9VwiX.jpg', 'ioo'),
(6, 36, 'pakaian', '85', '75', 'picture', 'wawa banget'),
(7, 36, 'pakaian q', '85', '75', 'picture', 'wawa banget'),
(8, 36, 'pakaian 11', '77', '55', 'ggg', 'sfdfdsf'),
(9, 36, 'kai', '85', '75', 'kai', 'ketrngaj'),
(11, 37, 'koko butik', '75', '65', 'Tapfu.jpg', 'bahus sekarlo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gerai`
--

CREATE TABLE `gerai` (
  `market_id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `nama_minimarket` varchar(255) NOT NULL,
  `alamat_minimarket` varchar(255) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `telp` varchar(50) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gerai`
--

INSERT INTO `gerai` (`market_id`, `user_id`, `nama_minimarket`, `alamat_minimarket`, `kota`, `telp`, `lat`, `lng`) VALUES
(3, 2, 'mini', 'malang', 'kota', '852444', 0, 0),
(4, 2, 'mono', 'malang', 'jakarta', '85244', 0, 0),
(5, 2, 'adik', 'kaka', 'gat tau', '', 0, 0),
(6, 2, 'lala', 'lala', 'lala', '852', 0, 0),
(7, 2, 'lolo', 'lolo', 'lolo', '85244', 0, 0),
(8, 2, 'mantap', 'mantap malang', 'malang', '08524452455555', 0, 0),
(9, 2, 'kurang mantap', 'kurang malang ', 'kurang', '05224568', 0, 0),
(10, 2, 'kurang mantap', 'kurang malang ', 'kurang', '05224568', 0, 0),
(11, 2, 'masih kurang ', 'maksi', '', '57', 0, 0),
(12, 2, 'banget', 'banget', 'kaskadj', '08524', 0, 0),
(13, 2, 'coba lagi ', 'mana tahu bisa ', 'moga aja ', '0854456', 0, 0),
(14, 2, 'ayp', 'ayp', 'ayo', '0854', 0, 0),
(15, 2, 'ahahhh', 'ahhhhhh', 'ahhhh', '085522', 0, 0),
(16, 2, 'ayolah ', 'came on ', 'goooo', '900000', 0, 0),
(32, 2, 'mama', 'mama', 'mama', '9090', 0, 0),
(36, 3, 'gusti jaya', 'lampung', 'lampung', '0852', -8.273633566817345, 114.78705359570313),
(37, 6, 'koko jaya', 'koko island', 'koko town', '0852', -8.226065772610774, 113.43573523632813),
(38, 2, 'kkks', 'adaa', 'sdsd', '0852', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` int(1) NOT NULL,
  `profile_image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `role`, `profile_image`) VALUES
(1, 'judica melanes hutabarat', 'judicahutabarat@gmail.com', '1ef942d56ca402c1d6e5cf5d1b005694', 2, ''),
(2, 'james', 'james@gmail.com', '0289101cc493508d1563c3ed963b3d04', 1, ''),
(3, 'gusti nyoman prasta', 'gusti@gmail.com', '337fb9e17d6f379971e9e8b06b2e8d5c', 2, 'bS0ru.jpg'),
(4, 'kaka', 'kaka@gmail.com', '273ccda37cd968f5652aa222a75cec0d', 2, './file_upload/Epse1.jpg'),
(5, 'lala', 'lala@gmail.com', '53d5803cb52f2f4a6fa9a0b8e730942c', 2, 'OENXv.jpg'),
(6, 'koko', 'koko@gmail.com', '3e08121aec7f1c114be192e64bbb1105', 2, 'YIdhs.jpg');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indeks untuk tabel `gerai`
--
ALTER TABLE `gerai`
  ADD PRIMARY KEY (`market_id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `gerai`
--
ALTER TABLE `gerai`
  MODIFY `market_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
