<?php include 'header_1.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-user" ></span> User</small></h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar_menu.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Form Edit Event -->
            <div class="panel panel-default" id="table-tna">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Edit Barang</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>

                <?php
                echo form_open_multipart('dashboard/edit_barang');
                ?>
                <!-- <?php foreach ($data as $d){}?> -->
                <div class="row">
                  </div>
                  <div class="row">
                    <div class="col-sm-6 form-horizontal">
                      <div class="form-group">
                        <label for="inputEvent" class="col-sm-2">Nama Barang</label>
                        <div class="col-sm-10">
                          <input type="hidden" name="id" value="<?php echo $d['id_barang']?>">
                          <input type="text" class="form-control" id="inputEvent" name="nama_barang" value="<?php echo $d['nama_barang']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEvent" class="col-sm-2">Harga Barang</label>
                        <div class="col-sm-10">

                          <input type="text" class="form-control" id="inputEvent" name="harga_barang" value="<?php echo $d['harga_barang']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEvent" class="col-sm-2">Harga Promo</label>
                        <div class="col-sm-10">

                          <input type="text" class="form-control" id="inputEvent" name="harga_promo" value="<?php echo $d['harga_promo']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEvent" class="col-sm-2">Image</label>
                        <div class="col-sm-10">
                          <input type="hidden" name="filelama" class="form-control" value="<?php echo $d['image'];?>">
                          <input type="file"  class="form-control" name="image">
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEvent" class="col-sm-2">Keterangan</label>
                        <div class="col-sm-10">

                          <input type="text" class="form-control" id="inputEvent" name="keterangan1" value="<?php echo $d['keterangan']?>">
                        </div>
                      </div>
                      </div>
                      <div class="col-sm-6 form-horizontal">

                      <img src="<?php echo base_url()?>file_upload/<?php echo $d['image'] ?>" alt="Foto" class="img-responsive foto-profil" width="100%" height="100%">
                  
                    </div>
                      <div class="row">
                        <div class="col-sm-11">
                          <div class="form-group">
                            <button type="button" onclick="goBack()" class="btn btn-default pull-right">Back</button>
                            <button name="submit" type="submit" class="btn btn-primary pull-right">Submit</button>
                          </div>
                        </div>
                      </div>
                      <?php
                      form_close(); 
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- END MAIN -->
        <?php include 'footer.php'; ?>
