<?php include 'header_1.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-user" ></span> User</small></h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar_menu.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Form Edit Event -->
            <div class="panel panel-default" id="table-jenjang">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Edit Gerai</h3>                
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <?php foreach ($data as $d){}?>
                <form class="form-horizontal" method="post" action="<?php echo base_url('admin/gerai_edit/').$d['market_id'];?>" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Nama Gerai</label>
                    <div class="col-sm-10">
                      <?php echo $d['nama_minimarket']?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Alamat Minimarket</label>
                    <div class="col-sm-10">
                      <?php echo $d['alamat_minimarket']?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Telepon</label>
                    <div class="col-sm-10">
                      <?php echo $d['telp']?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Kota</label>
                    <div class="col-sm-10">
                      <?php echo $d['kota']?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">latitude</label>
                    <div class="col-sm-10">
                      <?php echo $d['lat']?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">longitude</label>
                    <div class="col-sm-10">
                      <?php echo $d['lng']?>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div id="mapa" style="height: 400px;"></div>
                  </div> 
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="pull-right">
                        <a class="btn btn-danger" href="<?php echo base_url('admin/gerai');?>">Back</a>
                        <button type="button" class="btn btn-primary" id="btn-createJenjang" name="btn-createJenjang" ><span class="lnr lnr-pencil">Edit</span></button>
                        <!--  -->
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="panel panel-default" id="form-createJenjang" style="display: none;">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Edit Users</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>

                <?php
                echo form_open('dashboard/edit_gerai');
                ?>
                <!-- <?php foreach ($data as $d){}?> -->
                <div class="row">
                    <!-- <div class="col-sm-12">
                      <img style="margin: auto; margin-bottom: 10px; width: 150px;height: 200px;" src="<?php echo base_url().$d['image'] ?>"alt="Foto" class="img-responsive foto-profil">
                    </div> --> 
                  </div>
                  <div class="row">
                    <div class="col-sm-12 form-horizontal">
                      <div class="form-group">
                        <label for="inputEvent" class="col-sm-2">Nama Gerai</label>
                        <div class="col-sm-10">
                          <input type="hidden" name="id" value="<?php echo $d['market_id']?>">
                          <input type="text" class="form-control" id="inputEvent" name="nama_minimarket" value="<?php echo $d['nama_minimarket']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEvent" class="col-sm-2">Alamat Minimarket</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputEvent" name="alamat_minimarket" value="<?php echo $d['alamat_minimarket']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEvent" class="col-sm-2">Telepon</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputEvent" name="telepon" value="<?php echo $d['telp']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEvent" class="col-sm-2">Kota</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputEvent" name="kota" value="<?php echo $d['kota']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEvent" class="col-sm-2">latitude</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="lat" name="lat" value="<?php echo $d['lat']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEvent" class="col-sm-2">longitude</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="lng" name="lng" value="<?php echo $d['lng']?>">
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div id="map" style="height: 400px;"></div>
                      </div> 
                      <hr>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <button type="button" onclick="goBack()" class="btn btn-default">Back</button>
                            <button name="submit" type="submit" class="btn btn-primary pull-right">Submit</button>
                          </div>
                        </div>
                      </div>
                      <?php
                      form_close(); 
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- END MAIN -->
        <script>
          var defaultCenter = {
            lat : <?php echo $d['lat']?>, 
            lng :<?php echo $d['lng']?>
          };
          function initMap() {

            var map = new google.maps.Map(document.getElementById('mapa'), {
              zoom: 10,
              center: defaultCenter 
            });
            var marker = new google.maps.Marker({
              position: defaultCenter,
              map: map,
              title: 'Click to zoom',
              
            });
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 10,
              center: defaultCenter 
            });

            var marker = new google.maps.Marker({
              position: defaultCenter,
              map: map,
              title: 'Click to zoom',
              draggable:true
            });
            
            
            marker.addListener('drag', handleEvent);
            marker.addListener('dragend', handleEvent);
            
            var infowindow = new google.maps.InfoWindow({
              content: '<h4>Drag untuk pindah lokasi</h4>'
            });
            
            infowindow.open(map, marker);
          }

          function handleEvent(event) {
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('lng').value = event.latLng.lng();
          }

          $(function(){
            initMap();
          })
        </script>
        <?php include 'footer.php'; ?>
