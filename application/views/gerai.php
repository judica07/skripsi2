<?php include 'header_1.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-user" ></span>Gerai</small></h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar_menu.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Events -->
            <div class="panel panel-default" id="table-event">

              <div class="panel-heading main-color-bg">
                <?php 
                $id = $this->session->userdata('id');
                
                $query= $this->db->query("SELECT * FROM `gerai` WHERE user_id= '$id'")->num_rows();
                
                if ($query == 0) 
                {  
                  ?>
                  <a href="<?php echo base_url('dashboard/gerai_tambah')?>" class="btn btn-default btn-xs pull-right btn-create"><span class="fa fa-plus" aria-hidden="true"></span> </a>
                  <?php
                }
                ?>
                <h3 class="panel-title">Gerai</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <table id="table-event" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>Nama Minimarket</th>
                      <th>Alamat Minimarke</th>
                      <th>Telepon</th>
                      <th>Kota</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $x=1; foreach ($data as $d) {?>
                      <tr>
                        <td><?php echo $x ?></td>
                        <td><?php echo $d['nama_minimarket']?></td>
                        <td><?php echo $d['alamat_minimarket']?></td>
                        <td><?php echo $d['telp']?></td>
                        <td><?php echo $d['kota']?></td>
                        <td>
                          <a href="<?php echo base_url('dashboard/edit_gerai/').$d['market_id'];?>" class="btn btn-default btn-sm"><span class="lnr lnr-pencil">Detail</span> </a> 
                        </td>
                      </tr>
                      <?php $x++;};?>
                    </tbody>
                  </table>
                </div>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END MAIN -->
    <?php include 'footer.php'; ?>
