<?php include 'header_1.php' ?>
<!-- Header -->
<header id="header">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
       <h2><span class="fa fa-fw fa-cog fa-lg"></span>Dashboard</h2>
     </div>
   </div>
 </div>
</header>
<!-- End Header -->
<!-- Main -->
<section id="main">
  <div class="container">
    <div class="row">
      <?php include 'sidebar_menu.php' ?>
      <!-- Content -->
      <div class="col-md-9" id="main-content">
      </div>
      <!-- End Content -->
    </div>
  </div>
</section>
<!-- End Main -->
<?php include 'footer.php' ?>
