<!DOCTYPE html>
<html>
<head>
	<title>skripsi</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <!-- ASSETS -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/scrolling-nav/scrolling-nav.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/linearicons/style.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/DataTables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/metisMenu/mm-vertical.css">
  <!-- CUSTOM CSS -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/affiliate/styles.css">
	<!-- FAVICON -->
	<link rel="apple-touch-icon" href="<?php echo base_url() ?>assets/images/favicon.png">
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/images/favicon.png">
</head>
<body>
	<!-- Top Navbar -->
	<div id="top-navbar">
		<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="<?php echo base_url('index.php/dashboard')?>">
						<img src="<?php echo base_url() ?>assets/images/brand-logo.png" alt="Affiliate" class="img-responsive">
					</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="main-navbar">
					<ul class="nav navbar-nav navbar-right">
		        <li>
              <a href="<?php echo base_url('index.php') ?>">LOG IN</a>
            </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container -->
		</nav>
	</div>
