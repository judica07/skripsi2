<?php include 'header_1.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-user" ></span> User</small></h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar_menu.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Events -->
            <div class="panel panel-default" id="table-event">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Profile</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <table id="table-event" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>Username</th>
                      <th>Nama</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                    <?php $x=1; foreach ($data as $d) {?>
                      <tr>
                        <td><?php echo $x ?></td>
                        <td><?php echo $d['email']?></td>
                        <td><?php echo $d['username']?></td>
                        <td>
                          <a href="<?php echo base_url('dashboard/edit_profile/').$d['id'];?>" class="btn btn-default btn-sm"><span class="lnr lnr-pencil">Edit</span> </a>
                        </td>
                      </tr>
                      <?php $x++;};?>
                    </tbody>

                  </table>
                </div>
              </div>
              <!-- Form create events -->
              
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END MAIN -->
    <?php include 'footer.php'; ?>
