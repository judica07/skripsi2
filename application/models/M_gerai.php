<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Gerai extends CI_Model {
	//  function get_event($userid){		
	// 	return $this->db->query('SELECT e.id,em.membership_id, e.nama, e.harga, em.presentase,concat(ud.SLUG,e.slug)AS "slug" FROM event e, event_membership em, user_detail ud WHERE ud.id_membership=em.membership_id AND em.event_id=e.id AND e.disable=0 AND ud.user_id='.$userid);
	// }
	function get_gerai_admin($market_id=null){
		if ($market_id==null) {
			return $this->db->query('SELECT g.market_id,g.nama_minimarket,g.alamat_minimarket,g.kota,g.telp,g.lat,g.lng FROM gerai g');
		} 
		else{
			return $this->db->query('SELECT g.market_id,g.nama_minimarket,g.alamat_minimarket,g.kota,g.telp,g.lat,g.lng FROM gerai g WHERE g.market_id='.$market_id);
		}		
		
	}
	function get_barang_admin($id_barang=null){
		if ($id_barang==null) {
			return $this->db->query('SELECT b.id_barang,b.market_id,b.nama_barang,b.harga_barang,b.harga_promo,b.image,b.keterangan FROM barang b');
		} 
		else{
			return $this->db->query('SELECT b.id_barang,b.nama_barang,b.market_id,b.harga_barang,b.harga_promo,b.image,b.keterangan FROM barang b WHERE b.id_barang='.$id_barang);
		}		
		
	}
	function get_map(){
		this->db->query('SELECT g.lat,g.lng from gerai g');
		return $query->result_array();
	}
	function get_barang_dashboard($id){

		$this->db->select('b.nama_barang,b.harga_barang,b.harga_promo,b.image,b.keterangan,b.market_id,b.id_barang');
		$this->db->from('gerai g');
		$this->db->join('barang b', 'b.market_id = g.market_id');
		$this->db->where('g.user_id', $id);
		
		return $this->db->get();
		// if ($id_barang==null) {
		// 	return $this->db->query('SELECT b.id_barang,b.market_id,b.nama_barang,b.harga_barang,b.harga_promo,b.image,b.keterangan, g.market_id FROM barang b, gerai g WHERE b.market_id=g.market_id and user_id='.$id);
		// } 
		// else{
		// 	return $this->db->query('SELECT b.id_barang,b.nama_barang,b.market_id,b.harga_barang,b.harga_promo,b.image,b.keterangan FROM barang b WHERE b.id_barang='.$id_barang);
		// }		
		
	}

	function get_gerai_data($id){
		$query= $this->db->query('SELECT * FROM `user`, gerai WHERE user_id=id and  id='.$id);
		return $query->result_array();
	}
	public function getWhere($table, $id)
	{
		// return $this->db->get_where($table, $id)->num_rows();
		return $this->db->get_where($table, $id);
	}
	function tambah_barang($data){
		return $this->db->insert('barang', $data);
		if($this->db->affected_rows>0){
			return true;
		}
		else{
			return false;
		}

	}
	function create_gerai($data){
		return $this->db->insert('gerai', $data);
		if($this->db->affected_rows>0){
			return true;
		}
		else{
			return false;
		}

	}
	function update($table,$data,$id)
	{
		return $this->db->update($table,$data,$id);
	}
	function hapus($where,$table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
	
}
?>