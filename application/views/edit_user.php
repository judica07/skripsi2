<?php include 'header.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-user" ></span> User</small></h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Form Edit Event -->
            <div class="panel panel-default" id="table-tna">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Edit Users</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <?php foreach($data as $d) {?>
                <?php } ?>
                <form action="<?php echo base_url('index.php/admin/edit_users/'.$d['id']).'/y';?>" method="POST">
                  <div class="row"> 
                  </div>
                  <div class="row">
                    <div class="col-sm-7 form-horizontal">
                      <div class="form-group">
                        <label for="inputNamaLengkap" class="col-sm-4 control-label">Nama Lengkap</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" id="inputNamaLengkap" name="username" value="<?php echo $d['username'] ?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">Alamat Email</label>
                        <div class="col-sm-8">
                          <input type="email" class="form-control" id="inputEmail" name="email" value="<?php echo $d['email'] ?>"required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="inputTwitter">Role</label>
                          <?php if($d['role']== '2') { ?>
                          <select class="form-control" id="role" name="role">
                            <option value="1" >Admin</option>
                            <option value="2" selected>Member</option>
                          </select> 
                          <?php } else if ($d['role']=='1') { ?>
                          <select class="form-control" id="role" name="role">
                            <option value="1" selected>Admin</option>
                            <option value="2">Member</option>
                          </select> 
                          <?php } ?>
                        </div>
                      </div>
                    <hr>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <button type="button" onclick="goBack()" class="btn btn-default">Back</button>
                          <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END MAIN -->
    <?php include 'footer.php'; ?>
