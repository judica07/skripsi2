<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
      <link rel="stylesheet" href="<?php echo base_url ()?>assets-template/css/main.css"/>
      <noscript><link rel="stylesheet" href="<?php echo base_url ()?>assets-template/css/noscript.css" /></noscript>
    </head>
  </html>
