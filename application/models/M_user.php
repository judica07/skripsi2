<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {
	public function add_user($data){

		$this->db->insert('user', $data);
		if ($this->db->affected_rows()>0) {
			return true;
		} 
		else{
			return false;
		}
		
	}
	function cek_login($table,$where){
		return $this->db->get_where($table,$where);
	}
	function hash_password($username,$password){
		$hasil=md5($username.$password);
		return $hasil;
	}
	function get_user(){
		return $this->db->query('select u.id,u.email,u.username as nama_user from user u ')->result_array();
	}
	function get_user_data($id){
		$query= $this->db->query('SELECT * FROM `user` WHERE  id='.$id);
		return $query->result_array();
	}
	
	function update_user($table,$data,$where){
		$this->db->update($table,$data,$where);
		if ($this->db->affected_rows()>0) {
			return true;
		} 
		else{
			return false;
		}
	}
	function update($table,$data,$id)
	{
		return $this->db->update($table,$data,$id);
	}
}
?>