<?php include 'header.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-bullhorn" ></span> Barang Edit</h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Form Edit Event -->
            <div class="panel panel-default" id="table-tna">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Barang Edit</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <?php foreach ($data as $d){}?>
                <form class="form-horizontal" method="post" action="<?php echo base_url('index.php/admin/barang_edit/').$d['id_barang'];?>" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Nama Barang</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="nama_barang" value="<?php echo $d['nama_barang']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Harga Barang</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="harga_barang" value="<?php echo $d['harga_barang']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Harga Promo</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="harga_promo" value="<?php echo $d['harga_promo']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Image</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="image" value="<?php echo $d['image']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Keterangan </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="keterangan1" value="<?php echo $d['keterangan']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="pull-right">
                        <a class="btn btn-danger" href="<?php echo base_url('index.php/admin/barang');?>">Back</a>
                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END MAIN -->
  <?php include 'footer.php'; ?>
