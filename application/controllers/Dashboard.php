<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
		parent::__construct();

		if($this->session->userdata('status') == "login" && $this->session->userdata('role') == "2"){
		}
		else{
			redirect('user');
		}
	//$this->load->model('m_affiliate');
		$this->load->model('m_user');
		$this->load->model('m_gerai');
		$this->load->model('m_admin');
		$this->load->helper('string');
	}
	public function index()
	{
		$this->load->view('dashboard');
	}

	function gerai(){
		$id=$this->session->userdata('id');
		$data_gerai=$this->m_gerai->get_gerai_data($id);
		$this->load->view('gerai',array('data'=>$data_gerai));
	}
	function edit_gerai($market_id=null){

		if(isset($_POST['submit']))
		{
			$id = array('market_id' => $this->input->post("id",true) );
			$data = array(

				'nama_minimarket' => $this->input->post("nama_minimarket",true),
				'alamat_minimarket' => $this->input->post("alamat_minimarket",true),
				'telp' => $this->input->post("telepon",true),
				'kota' => $this->input->post("kota",true),
				'lat' => $this->input->post("lat",true),
				'lng' => $this->input->post("lng",true),
			);
			$query = $this->m_gerai->update('gerai',$data,$id);
			if($query){
				redirect('dashboard/gerai');
			}

		}
		else
		{
		//$market_id = $this->uri->segment(3);
			$data=$this->m_gerai->get_gerai_admin($market_id)->result_array();
			$this->load->view('edit_gerai',array('data'=>$data));

		}

	}
	function edit_barang($id_barang=null){

		if(isset($_POST['submit']))
		{

			$config = array(
				'file_name' => random_string('alnum',5),
				'upload_path' => './file_upload/',
				'allowed_types' => 'jpg|png',
				'max_size' => '2000',
				'max_width' => '3700',
				'max_height' => '3700',
				'overwrite' => TRUE
			);
			$filelama   = $this->input->post('filelama');

			$this->load->library('upload', $config);

			if($_FILES['image']['name'])
			{
				if (!$this->upload->do_upload('image'))
				{

					$error = $this->upload->display_errors('<p>', '</p>');


					echo $error;
				}
				else{
					$image = $this->upload->data('file_name');
					$path = $this->upload->data('file_path');
					$id = array('id_barang' => $this->input->post("id",true) );
					$data = array(

						'nama_barang' => $this->input->post("nama_barang",true),
						'harga_barang' => $this->input->post("harga_barang",true),
						'harga_promo' => $this->input->post("harga_promo",true),
						'image' => $image,
						'keterangan' => $this->input->post("keterangan1",true),
					);
					$query = $this->m_gerai->update('barang',$data,$id);
					if($query){
						redirect('dashboard/barang');
					}
				}
			}
			else{
				$id = array('id_barang' => $this->input->post("id",true) );
					$data = array(

						'nama_barang' => $this->input->post("nama_barang",true),
						'harga_barang' => $this->input->post("harga_barang",true),
						'harga_promo' => $this->input->post("harga_promo",true),
						'keterangan' => $this->input->post("keterangan1",true),
					);
					$query = $this->m_gerai->update('barang',$data,$id);
					if($query){
						redirect('dashboard/barang');
					}
			}
			

		}
		else
		{
			$data=$this->m_gerai->get_barang_admin($id_barang)->result_array();
			$this->load->view('barang_edit',array('data'=>$data));
		}

	}
	function hapus_barang($id_barang){
		$where = array('id_barang' => $id_barang);
		$this->m_gerai->hapus($where,'barang');
		if($this->db->affected_rows()>0){
			$this->session->set_flashdata('pesan','Data Berhasil Dihapus.');
			redirect('dashboard/barang');
		}
		else{
			$this->session->set_flashdata('pesan','Data gagal dihapus.');
			redirect('dashboard/barang');
		}
	}
	function gerai_tambah(){

		$this->load->view('tambah_gerai');
	}
	function barang_tambah(){

		$this->load->view('tambah_barang');
	}
	public function profile()
	{
		$id=$this->session->userdata('id');
		$data_user=$this->m_user->get_user_data($id);
		$this->load->view('profile',array('data'=>$data_user));
	}
	function edit_profile(){
		if(isset($_POST['submit'])) {
			$config = array(
				'file_name' => random_string('alnum',5),
				'upload_path' => './file_upload/',
				'allowed_types' => 'jpg|png',
				'max_size' => '2000',
				'max_width' => '3700',
				'max_height' => '3700',
				'overwrite' => TRUE
			);
			$filelama   = $this->input->post('filelama');

			$this->load->library('upload', $config);


			if($_FILES['userfile']['name'])
			{
				if (!$this->upload->do_upload('userfile'))
				{

					$error = $this->upload->display_errors('<p>', '</p>');


					echo $error;
				}
				else
				{

					$foto = $this->upload->data('file_name');
					$path = $this->upload->data('file_path');
					$data = array(
						'profile_image' => $foto,

						'username' => $this->input->post("username", true),
						'email' => $this->input->post("email", true)

					);
				 @unlink($path.$filelama);//menghapus gambar lama, variabel dibawa dari form


				 $id = array(
				 	'id' => $this->input->post("id", true),
				 );
				// $res2=$this->m_user->update_user('user',$data_user, array('id'=> $id));

				 $query = $this->m_user->update('user', $data, $id);
				 if ($query) {



				 	redirect('dashboard/profile');
				 }

				}
			}
			else{
				$data = array(
					

					'username' => $this->input->post("username", true),
					'email' => $this->input->post("email", true)

				);
				$id = array(
					'id' => $this->input->post("id", true),
				);
				$query = $this->m_user->update('user', $data, $id);
				if ($query) {

					
					
					redirect('dashboard/profile');
				}
			}
		}
		else{
			$data=$this->m_user->get_user_data($this->uri->segment(3));
			$this->load->view('edit_profile',array('data'=>$data));

		}

	}
	function create_gerai(){
		$user_id 	= $userid=$this->session->userdata('id');

		$data_insert				= array(
			'user_id'			=> $user_id,
			'nama_minimarket'	=> $this->input->post('nama_minimarket'),
			'alamat_minimarket'	=> $this->input->post('alamat_minimarket'),
			'kota'				=> $this->input->post('kota'),
			'telp'				=> $this->input->post('telp'),
			'lat'				=> $this->input->post('lat'),
			'lng'				=> $this->input->post('lng')

		);

		$res = $this->m_admin->create_gerai($data_insert);

		if ($res) {
			redirect('dashboard/gerai'); //ke view halaman terimakasih
		} else {
			$this->session->set_flashdata('pesan','Isi Semua Form Dengan Benar!');
			redirect('dashboard/gerai');
		}

	}
	function barang($id_barang=null){
		$id=$this->session->userdata('id');
		if ($id_barang==null) {
			$get_barang=$this->m_gerai->get_barang_dashboard($id)->result_array();
			$this->load->view('barang',array( 'data'=>$get_barang));
		} else {
			$get_barang=$this->m_gerai->get_barang_admin($id_barang)->result_array();
			$this->load->view('dashboard/barang_edit',array('data'=>$get_barang));
		}
	}
	function tambah_barang(){
		$this->load->library('upload');
		$config['upload_path']          = './file_upload/';
		$config['allowed_types']        = 'gif|jpg|png|';
		$config['file_name'] = random_string('alnum',5);
		$this->upload->initialize($config);

		if($_FILES['foto_barang']['size']!=0)
		{
			if ($this->upload->do_upload('foto_barang'))
			{
				$data_image = $this->upload->data();
				$foto_barang=$data_image['file_name'];
				$id = array(
					'user_id' => $this->session->userdata('id')
				);
				$data_gerai = $this->m_gerai->getWhere('gerai',$id)->row_array();

				$data_insert= array(
					'market_id' => $data_gerai['market_id'],
					'nama_barang' => $this->input->post('nama_barang'),
					'harga_barang' => $this->input->post('harga_barang'),
					'harga_promo' => $this->input->post('harga_promo'),
					'image' => $foto_barang,
					'keterangan' => $this->input->post('keterangan1'),
				);


				$res = $this->m_gerai->tambah_barang($data_insert);

				if ($res){
					redirect('dashboard/barang');
				}
				else{
					$this->session->$this->session->set_flashdata('pesan', 'Isi Semua Form Dengan Benar!');
					redirect('dashboard/barang');
				}
			}

			
			
			

		}
	}

}
