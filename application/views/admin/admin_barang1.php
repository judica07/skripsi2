<?php include 'header.php'; ?>
<!-- HEADER -->
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <h2><span class="lnr lnr-bullhorn" ></span> Barang</h2>
      </div>
    </div>
  </div>
</header>
<!-- END HEADER -->
<!-- MAIN -->
<section id="main-content">
  <div class="container">
    <div class="row">
      <?php include 'sidebar.php'; ?>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <!-- Events -->
            <div class="panel panel-default" id="table-event">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">barang</h3>
              </div>
              <div class="panel-body">
                <p style="color:#ff6666"> <?php echo $this->session->flashdata('pesan') ?></p>
                <table id="table-event-1" class="table table-striped table-bordered" width="100%">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>Market id</th>
                      <th>Nama barang</th>
                      <th>harga barang</th>
                      <th>harga promo</th>
                      <th>image</th>
                      <th>keterangan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $x=1; foreach ($data as $d) {?>
                      <tr>
                        <td><?php echo $x ?></td>
                        <td><?php echo $d['market_id']?></td>
                        <td><?php echo $d['nama_barang']?></td>
                        <td><?php echo $d['harga_barang']?></td>
                        <td><?php echo $d['harga_promo']?></td>
                        <td><?php echo $d['image']?></td>
                        <td><?php echo $d['keterangan']?></td>
                        <td>
                          <a href="<?php echo base_url('admin/barang/').$d['id_barang'];?>" class="btn btn-default btn-sm"><span class="lnr lnr-pencil">Edit</span> </a>
                          <a href="<?php echo base_url('admin/hapus_barang/').$d['id_barang'];?>" onclick="return confirm('Yakin Ingin Menghapus Barang ini ?')" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove">Hapus</span> </a>
                          

                        </td>
                      </tr>
                      <?php $x++;};?>
                    </tbody>

                  </table>
                </div>
              </div>
              <!-- Form create events -->
            <!-- <div class="panel panel-default" id="form-createEvent" style="display: none;">
              <div class="panel-heading">
                <div class="panel-title">
                  Create an gerai
                </div>
              </div>
              <div class="panel-body">
                <form class="form-horizontal" method="post" action="<?php echo base_url('index.php/admin/create_gerai');?>" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Nama Minimarket</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="nama_minimarket" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Alamat Minimarket</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="alamat_minimarket" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Telepon</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="telp" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEvent" class="col-sm-2">Kota</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEvent" name="kota" >
                    </div>
                  </div>
                  
                  
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="button" class="btn btn-danger" id="btn-createEventCancel">Back</button>
                      <div class="pull-right">
                        <button type="reset" class="btn btn-warning" id="btn-createEventReset" name="submit">Reset</button>
                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END MAIN -->
  <?php include 'footer.php'; ?>
