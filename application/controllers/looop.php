	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboard extends CI_Controller {
		function __construct(){
			parent::__construct();

			if($this->session->userdata('status') == "login" && $this->session->userdata('role') == "2"){
			}
			else{
				redirect('user');
			}
			//$this->load->model('m_affiliate');
			$this->load->model('m_user');
			$this->load->model('m_gerai');
			$this->load->model('m_admin');
			$this->load->helper('string');
		}
		public function index()
		{
			$this->load->view('dashboard');
		}

		function gerai(){
			$id=$this->session->userdata('id');
			$data_gerai=$this->m_gerai->get_gerai_data($id);
			$this->load->view('gerai',array('data'=>$data_gerai));
		}
		function edit_gerai($market_id=null){
			
			if(isset($_POST['submit']))
			{
				$id = array('market_id' => $this->input->post("id",true) );
				$data = array(

					'nama_minimarket' => $this->input->post("nama_minimarket",true),
					'alamat_minimarket' => $this->input->post("alamat_minimarket",true),
					'telp' => $this->input->post("telepon",true),
					'kota' => $this->input->post("kota",true),
				);
				$query = $this->m_gerai->update('gerai',$data,$id);
				if($query){
					redirect('dashboard/gerai');
				}

			}
			else
			{
				//$market_id = $this->uri->segment(3);
				$data=$this->m_gerai->get_gerai_admin($market_id)->result_array();
				$this->load->view('edit_gerai',array('data'=>$data));
				

			}

		}
		function edit_barang($id_barang=null){
			
			if(isset($_POST['submit']))
			{
				$id = array('id_barang' => $this->input->post("id",true) );
				$data = array(

					'nama_barang' => $this->input->post("nama_barang",true),
					'harga_barang' => $this->input->post("harga_barang",true),
					'harga_promo' => $this->input->post("harga_promo",true),
					'image' => $this->input->post("image",true),
					'keterangan' => $this->input->post("keterangan1",true),
				);
				$query = $this->m_gerai->update('barang',$data,$id);
				if($query){
					redirect('dashboard/barang');
				}

			}
			else
			{
				//$market_id = $this->uri->segment(3);
				$data=$this->m_gerai->get_barang_admin($id_barang)->result_array();
				$this->load->view('barang_edit',array('data'=>$data));
				

			}

		}
		function gerai_tambah(){

			$this->load->view('tambah_barang');
		}
		public function profile()
		{
			$id=$this->session->userdata('id');
			$data_user=$this->m_user->get_user_data($id);
			$this->load->view('profile',array('data'=>$data_user));
		}
		function edit_profile($id,$edit=null){
			if ($edit==null){
				$data=$this->m_user->get_user_data($id);
				
				$this->load->view('edit_profile',array('data'=>$data));
			} else if($edit=='y'){
				
				$data_user=array(
					'username'=>$_POST['username'],
					'email'=>$_POST['email']
					// 'role'=>$_POST['role']
					// 'role'=>$_POST['role']
				);
				// $res1=$this->m_user->update_user('user_detail',$data_user_detail, array('user_id'=> $id));
				$res2=$this->m_user->update_user('user',$data_user, array('id'=> $id));
				if ($res2) {
					$this->session->set_flashdata('pesan','Data Berhasil Diperbarui');
					redirect(base_url('dashboard/edit_profile/'.$id));
				} else {
					$this->session->set_flashdata('pesan','Data Gagal Diperbarui');
					redirect(base_url('dashboard/edit_profile/'.$id));
				}
				
			}
		}
		function create_gerai(){
			$user_id 	= $userid=$this->session->userdata('id');

			$data_insert				= array(
				'user_id'			=> $user_id,
				'nama_minimarket'	=> $this->input->post('nama_minimarket'),
				'alamat_minimarket'	=> $this->input->post('alamat_minimarket'),
				'kota'				=> $this->input->post('kota'),
				'telp'				=> $this->input->post('telp'),
				'lat'				=> $this->input->post('lat'),
				'lng'				=> $this->input->post('lng')
				
			);

			$res = $this->m_admin->create_gerai($data_insert);
			
			if ($res) {
					redirect('dashboard/gerai'); //ke view halaman terimakasih
				} else {
					$this->session->set_flashdata('pesan','Isi Semua Form Dengan Benar!');
					redirect('dashboard/gerai');
				}

			}
			function barang($id_barang=null){
				$id=$this->session->userdata('id');
				if ($id_barang==null) {
					$get_barang=$this->m_gerai->get_barang_dashboard($id)->result_array();
					$this->load->view('barang',array( 'data'=>$get_barang));
				} else {
					$get_barang=$this->m_gerai->get_barang_admin($id_barang)->result_array();
					$this->load->view('dashboard/barang_edit',array('data'=>$get_barang));
				}
			}
			function tambah_barang(){

				$id = array(
					'user_id' => $this->session->userdata('id')
				);
				$data_gerai = $this->m_gerai->getWhere('gerai',$id)->row_array();



				$data_insert= array(
					'market_id' => $data_gerai['market_id'],
					'nama_barang' => $this->input->post('nama_barang'),
					'harga_barang' => $this->input->post('harga_barang'),
					'harga_promo' => $this->input->post('harga_promo'),
					'image' => $this->input->post('image'),
					'keterangan' => $this->input->post('keterangan1'),
				);
				$res = $this->m_gerai->tambah_barang($data_insert);

				if ($res){
					redirect('dashboard/barang');
				}
				else{
					$this->session->$this->session->set_flashdata('pesan', 'Isi Semua Form Dengan Benar!');
					redirect('dashboard/barang');
				}
			}
		}