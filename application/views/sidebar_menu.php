<div id="sidebar" class="col-md-3">
  <!-- Sidebar -->
  <div class="list-group">
    <a href="<?php echo base_url('dashboard') ?>" class="list-group-item">
      <span class="lnr lnr-cog"></span> Dashboard
    </a>
    <a href="<?php echo base_url('dashboard/profile') ?>" class="list-group-item">
      <span class="lnr lnr-users"></span> profile
    </a>
    <a href="<?php echo base_url('dashboard/gerai') ?>" class="list-group-item">
      <span class="lnr lnr-bullhorn"></span> Gerai
    </a>
    <a href="<?php echo base_url('dashboard/barang') ?>" class="list-group-item">
      <span class="lnr lnr-file-add"></span> Barang
    </a>
    
  </div>
</div>
